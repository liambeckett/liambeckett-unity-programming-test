﻿using UnityEngine;

public class Brick : MonoBehaviour {

    void OnCollisionEnter(Collision other)
    {
        GamePlayController.instance.BrickDetroyed();
        Destroy(gameObject);
    }
}
