﻿
using UnityEngine;

public class Paddle : MonoBehaviour {

    public float paddleSpeed = 1f;

    private Vector3 paddelPosition;

    private void Awake()
    {
        paddelPosition = transform.position;
    }

    void Update()
    {
        float xPos = transform.position.x + (Input.GetAxis("Horizontal") * paddleSpeed);
        paddelPosition = new Vector3(Mathf.Clamp(xPos, -4.5f, 4.5f), -4.5f, 0.0f);

        transform.position = Vector2.Lerp(transform.position, paddelPosition, paddleSpeed);
    }
}
