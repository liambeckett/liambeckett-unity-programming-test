﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    private GameController gameController;

    [SerializeField] Text titleText;
    [SerializeField] Text buttonText;

	void Start ()
    {
        gameController = GameController.instance;
        titleText.text = gameController.GetMenuTitleText();
        buttonText.text = gameController.GetMenuButtonText();
	}

    /// <summary>
    /// Method to start the game (Used by the start button).
    /// </summary>
    public void StartGame() { gameController.StartGamePlay(); }
	
}
