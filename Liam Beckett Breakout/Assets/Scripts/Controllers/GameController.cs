﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameState { START_SCREEN, PLAYING, GAME_OVER }

public class GameController : MonoBehaviour {

    public static GameController instance = null;

    private GameState state;

    private const string startScreenText = "WELCOME TO BREAKOUT!";
    private const string gameOverScreenText = "GAME OVER!";

    private const string startScreenButtonText = "START GAME";
    private const string gameOverScreenButtonText = "RESTART";

    private const int startingLives = 3;
    private int numberOfLivesLeft;

    private int startingNumberOfBricks;
    private int level;
    
    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);
        
        DontDestroyOnLoad(gameObject);

        startingNumberOfBricks = 3;
    }

    private void Start()
    {
        state = GameState.START_SCREEN;
        ChangeScene(1);
    }

    /// <summary>
    /// Method to trigger the start of the game play.
    /// </summary>
    public void StartGamePlay()
    {
        state = GameState.PLAYING;
        numberOfLivesLeft = startingLives;
        level = 0;
        ChangeScene(2);
    }

    /// <summary>
    /// Method to trigger the end of the game and the restart screen.
    /// </summary>
    private void GameOver()
    {
        state = GameState.GAME_OVER;
        ChangeScene(1);
    }

    /// <summary>
    /// Method which updates the game object to reflect a life being lost.
    /// </summary>
    public void LooseLife()
    {
        numberOfLivesLeft--;
        if (numberOfLivesLeft <= 0)
        {
            GameOver();
        }
    }

    /// <summary>
    /// Method which updates the game object to reflect the next level.
    /// </summary>
    public void NextLevel()
    {
        level++;
        numberOfLivesLeft++;
    }

    /// <summary>
    /// Method to get the number of bricks which should be in the current level.
    /// </summary>
    /// <returns> The number of bricks </returns>
    internal int GetNumberOfBricks()
    {
        return startingNumberOfBricks + level * 2;
    }

    /// <summary>
    /// Changes the scene to the specified index which relates to build index of the scene in Unity
    /// </summary>
    /// <param name="index"> The build index of the desired scene </param>
    private void ChangeScene(int index) { SceneManager.LoadScene(index); }

    /// <summary>
    /// Gets the correct Button text for the Game Menu based on the game state.
    /// </summary>
    /// <returns> The button text </returns>
    internal string GetMenuButtonText()
    {
        switch (state)
        {
            case GameState.START_SCREEN:
                return startScreenButtonText;
            case GameState.GAME_OVER:
                return gameOverScreenButtonText;
            // This point should not be reached.
            default:
                return "";
        }
    }

    /// <summary>
    /// Gets the correct Title text for the Game Menu based on the game state.
    /// </summary>
    /// <returns> The title text </returns>
    internal string GetMenuTitleText()
    {
        switch (state)
        {
            case GameState.START_SCREEN:
                return startScreenText;
            case GameState.GAME_OVER:
                return gameOverScreenText;
            // This point should not be reached.
            default:
                return "";
        }
    }

    /// <summary>
    /// Gets the string which represents the current number of lives left.
    /// </summary>
    /// <returns> The lives left text </returns>
    internal string GetLivesText()
    {
        switch (state)
        {
            case GameState.PLAYING:
                return "Lives: " + numberOfLivesLeft;
            // This point should not be reached.
            default:
                return "";
        }
    }

    /// <summary>
    /// Gets the string which represents the current number of level.
    /// </summary>
    /// <returns> The current level text </returns>
    internal string GetLevelText()
    {
        switch (state)
        {
            case GameState.PLAYING:
                return "Level: " + (level + 1);
            // This point should not be reached.
            default:
                return "";
        }
    }
}
