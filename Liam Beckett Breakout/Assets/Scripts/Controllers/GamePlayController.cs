﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayController : MonoBehaviour
{
    public static GamePlayController instance = null;

    [SerializeField] Text numberOfLivesText;
    [SerializeField] Text levelText;

    [SerializeField] GameObject brickPrefab;

    [SerializeField] Vector2 TopLeftBoundary;
    [SerializeField] Vector2 BottomRightBoundary;

    private GameController gameController;

    private List<Vector3> validBrickLocations;
    private int numberOfBricksInLevel;

    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        gameController = GameController.instance;

        CalculateValidBrickLocations();
    }

    private void Start()
    {
        UpdateLivesText();
        UpdateLevelText();

        LoadLevel();
    }
    
    /// <summary>
    /// Method to ensure the text displaying the current number of lives is correct.
    /// </summary>
    public void UpdateLivesText() { numberOfLivesText.text = gameController.GetLivesText(); }

    /// <summary>
    /// Method to ensure the text displaying the current level is correct.
    /// </summary>
    public void UpdateLevelText() { levelText.text = gameController.GetLevelText(); }

    /// <summary>
    /// Method to log a brick being destroyed. Additionally checks to see if the level has been completed.
    /// </summary>
    internal void BrickDetroyed()
    {
        numberOfBricksInLevel--;

        // Check it wasnt the last brick.
        if (numberOfBricksInLevel <= 0)
        {
            gameController.NextLevel();
            UpdateLevelText();
            LoadLevel();
        }
    }
    
    /// <summary>
    /// Utility function to calculate all the vaild postion a brick can spawn in. Requires the top left and bottom right points for the 
    /// valid spawn area to be defined and assumes the map is symetrical along the x = 0 line.
    /// </summary>
    private void CalculateValidBrickLocations()
    {
        List<float> validBrickYLocations = new List<float>();
        validBrickLocations = new List<Vector3>();

        float brickWidth = brickPrefab.transform.localScale.x;
        float brickHeight = brickPrefab.transform.localScale.y;
        float gapBetweenBricks = 0.25f;

        // First find all the valid y positions.
        for (float y = TopLeftBoundary.y - (gapBetweenBricks + 0.5f * brickHeight); y + 0.5f* brickHeight > BottomRightBoundary.y; y -= (gapBetweenBricks + brickHeight))
        {
            validBrickYLocations.Add(y);
        }

        // Assumes the horizontal center of the level is at 0 and finds the valid x positions in one direction from zero, and
        // adds the complete vector (including constant z) to the list of locations.
        for (float x = 0; x + 0.5f * brickWidth < BottomRightBoundary.x; x += (gapBetweenBricks + brickWidth))
        {
            foreach (float y in validBrickYLocations)
            {
                validBrickLocations.Add(new Vector3(x, y, 0.0f));
                validBrickLocations.Add(new Vector3(-x, y, 0.0f));
            }
        }
    }

    /// <summary>
    /// Method which loads in the level, instaniating all the required brick prefabs.
    /// </summary>
    private void LoadLevel()
    {
        List<Vector3> copyOfBrickLocations = new List<Vector3>(validBrickLocations);
        int spawnedBricks = 0;
        bool maxBricksSpawned = false;

        Debug.Log(copyOfBrickLocations.Count);

        numberOfBricksInLevel = gameController.GetNumberOfBricks();

        while (spawnedBricks < numberOfBricksInLevel && !maxBricksSpawned)
        {
            int index = UnityEngine.Random.Range(0, copyOfBrickLocations.Count-1);

            Vector3 spawnLocation = copyOfBrickLocations[index];
            Instantiate(brickPrefab, spawnLocation, Quaternion.identity);

            copyOfBrickLocations.Remove(spawnLocation);
            spawnedBricks++;

            // Check we havnt loaded as many as we can.
            if (spawnedBricks >= validBrickLocations.Count) maxBricksSpawned = true;
        }

    }
}
