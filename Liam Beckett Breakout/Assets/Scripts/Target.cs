﻿using UnityEngine;

public class Target : MonoBehaviour {

    private Vector3 mousePosition;
    private Vector3 adjustedPosition;

    public float moveSpeed = 5.0f;

    void Update()
    {
        // Adjust the targets position to be inline with the mouse on the x axis, keeping the target within the gameplay area.
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        adjustedPosition = new Vector3(Mathf.Clamp(mousePosition.x, -5.5f, 5.5f), Mathf.Clamp(mousePosition.y, -3.5f, -1.5f), 0.0f);
        transform.position = adjustedPosition;
    }
}
