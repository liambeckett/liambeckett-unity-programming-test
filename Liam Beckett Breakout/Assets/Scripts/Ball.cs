﻿using UnityEngine;

public class Ball : MonoBehaviour {

    [SerializeField] float intialBallVelocity;
    [SerializeField] GameObject target;
    [SerializeField] GameObject paddle;

    private Rigidbody rigidbody;
    private bool inPlay;

    private Vector3 startingLocation;

    private float previousX;
    private float previousY;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        startingLocation = transform.position;
    }

    private void Start()
    {
        InitializeBall();
    }

    /// <summary>
    /// Method which initializes all the balls variables.
    /// </summary>
    private void InitializeBall()
    {
        inPlay = false;

        previousX = 10.0f;
        previousY = 10.0f;

        transform.position = startingLocation;
        rigidbody.velocity = new Vector3(0, 0, 0);
    }

    void Update () {
        
        if (!inPlay)
        {
            AdjustPositionToMatchPaddle();

            if (Input.GetButtonDown("Fire1"))
            {
                Vector3 direction = (target.transform.position - rigidbody.transform.position).normalized;

                inPlay = true;

                rigidbody.AddForce(direction * intialBallVelocity);
            }
        }
        else
        {
            // We dont want the ball to get stuck in a vertical or horizontal bounce lopp.
            if (transform.position.y == previousY) rigidbody.AddForce(new Vector3(0, UnityEngine.Random.Range(-15.0f, 15.0f), 0));
            if (transform.position.x == previousX) rigidbody.AddForce(new Vector3(UnityEngine.Random.Range(-15.0f, 15.0f), 0, 0));
            LogPreviousCoordinates();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DeadZone")
        {
            GameController.instance.LooseLife();
            GamePlayController.instance.UpdateLivesText();
            InitializeBall();
        }
    }

    /// <summary>
    ///  Method to adjust the x position of the ball when the ball is not in play so that it travels with
    ///  the paddle.
    /// </summary>
    private void AdjustPositionToMatchPaddle()
    {
        // Need to make sure this isnt used at the wrong time.
        if (!inPlay)
        {
            var paddleLocation = paddle.transform.position;
            var ballLocation = transform.position;

            ballLocation.x = paddleLocation.x;

            transform.position = ballLocation;
        }
    }

    /// <summary>
    /// Method which keeps track of the balls latest coordinates.
    /// </summary>
    private void LogPreviousCoordinates()
    {
        previousX = transform.position.x;
        previousY = transform.position.y;
    }
}
