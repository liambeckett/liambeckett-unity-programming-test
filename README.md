# README #

## How to Run My Version ##

In order to run this game in the Unity Editor the user should have the _preload scene open and run the program from there.

Additionally, ensure that the 3 scenes have been added in the Build Settings as the scripts reference the scene index for swapping between scenes. 
The order should be _preload(0), Menu(1) and GamePlay(2).

### Playing the Game ###

There are instruction on how to play in the Game itself, but for completeness they will be here too.

Before the ball has been fired the user can move the paddle and the ball with the LEft and Right arrow keys. Once the ball has been fired only the paddle will move with these keys.

To fire the ball use the mouse to aim the target and line up your shot. When aimed in the correct direction, click the Left mouse button to fire.

### The Game ###

The player starts with a few bricks to break, and this number will increase as the player clears each screen and advances to another level or all lives are lost. Each time a level is cleared
the user is given another live. The bricks are randomly placed for each level and each game.

## Instructions ##

### Breakout ###
We are going old school and want to test your skills at creating a Breakout game.  There are a number of breakout style games available on the internet for you to test out.
Don't spend too long on this. 3-4 hours is more than enough.

### Functional Requirements ###
1. The ball should rebound off the sides, top of the game board and off the paddle. 
2. If the ball falls off the bottom of the board the game is over or a life is lost.
3. The game should play from the Unity Editor.
4. The game should be re-playable without closing the game.

### What we're looking for ###
* The process you used to complete this test
* Prudent use of Object Orientated design
* Code reusability
* Extensibility and maintainability of the software
* Use of appropriate Unity capabilities
* Use of best practises
* Your creativity in making the game fun

### Deliverables ###
* Instructions on how to run and play your game
* Code should be committed to a git repository hosted on [bitbucket.org](https://bitbucket.org)

### Extra Credit ###
* Can be played in both orientations; landscape and portrait
* Can be installed as an app
* Any extra features you want to include in the game

## Technology ##
You can use assets (excluding code) from the Unity Asset Store, however please note what assets you have used and why you have used them.

## How do I get set up? ##
* [Fork](../../fork) this repository to your bitbucket account, then clone it to your local machine
* Create a new Unity project in the repository. You should use a recent version of Unity (5.6 or later).

## Next Steps ##
After you have finished your submission, make sure the reviewers have read access to your repository. Our developers will review your submission and then invite you in for a discussion.